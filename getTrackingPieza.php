<?php

/*
Api - 23/8/2018
Para MD

Hecho por Federico Grant


recibe y devuelve un Json

comentarios: no se pudo usar funciones predefinidas para parsear porque Oca usa ASP y tiene un dataset complicado que no le encontre la vuelta sin que devuelva errores.
*/

class getTrackingPieza {
			
	function __construct() {

		//wsdl 
		$url = "http://webservice.oca.com.ar/ePak_tracking/Oep_TrackEPak.asmx?wsdl";

		//capturar la info del JSON 
		$data = $this->getData();

		//armar el soap, el 1.2 no salio, es el 1.1 
		$body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
		<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
		  <soap:Body>
		    <Tracking_Pieza xmlns=\"#Oca_e_Pak\">";
		      if(isset($data['NroDocumentoCliente'])){ $body .= "<NroDocumentoCliente>".$data['NroDocumentoCliente']."</NroDocumentoCliente>";}
		      if(isset($data['CUIT'])){ $body .= "<CUIT>".$data['CUIT']."</CUIT>";}
		$body .= "
		      <Pieza>".$data['Pieza']."</Pieza>
		    </Tracking_Pieza>
		  </soap:Body>
		</soap:Envelope>";

		$headers = array(
			"POST /epak_tracking/Oep_TrackEPak.asmx HTTP/1.1",
			"Host: webservice.oca.com.ar",
			"Content-type:  text/xml; charset=\"utf-8\"",
			"SOAPAction: \"#Oca_e_Pak/Tracking_Pieza\"",
			"Content-length: ".strlen($body)
		);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

		$output = curl_exec($ch);
		curl_close($ch);

		//armo el json para devolver
		header('Content-Type: application/json');
		echo $this->parser($output);

	}

	// Parseador Custom para el Dataset de asp de oca. 
	function parser($body) {

		$elements = Array();

		$NewDataSet = explode('<NewDataSet xmlns="">', $body);

		if(count($NewDataSet)>1){
			$NewDataSet = explode('</NewDataSet>', $NewDataSet[1]);

			if(count($NewDataSet)>1){
				$elements[] = trim($NewDataSet[0]);
			}
		}

		$dividedElements = explode("<Table", $elements[0]);
		$countedElements = count($dividedElements);

		$array = Array();

		for ($i=1; $i < $countedElements; $i++) { 

			$NumeroEnvio = explode('<NumeroEnvio>', $dividedElements[$i]);
			$NumeroEnvioEnd = explode('</NumeroEnvio>', $NumeroEnvio[1]);

			$Descripcion_Motivo = explode('<Descripcion_Motivo>', $dividedElements[$i]);
			$Descripcion_MotivoEnd = explode('</Descripcion_Motivo>', $Descripcion_Motivo[1]);

			$Desdcripcion_Estado = explode('<Desdcripcion_Estado>', $dividedElements[$i]);
			$Desdcripcion_EstadoEnd = explode('</Desdcripcion_Estado>', $Desdcripcion_Estado[1]);

			$SUC = explode('<SUC>', $dividedElements[$i]);
			$SUCEnd = explode('</SUC>', $SUC[1]);

			$fecha = explode('<fecha>', $dividedElements[$i]);
			$fechaEnd = explode('</fecha>', $fecha[1]);

			$array[$i]['NumeroEnvio'] = $NumeroEnvioEnd[0];
			$array[$i]['Descripcion_Motivo'] = $Descripcion_MotivoEnd[0];
			$array[$i]['Descripcion_Estado'] = $Desdcripcion_EstadoEnd[0];
			$array[$i]['SUC'] = $SUCEnd[0];
			$array[$i]['fecha'] = $fechaEnd[0];

			//var_dump($array[$i]['fecha']);die();

		}

		return (json_encode($array));

	}

	// funcion para capturar
	function getData() {
		$body = file_get_contents('php://input');
		$post = json_decode($body,true);
		return $post;
	}

}

// inicializo la clase
$test = new getTrackingPieza();

?>